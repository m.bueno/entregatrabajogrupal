import inmuebles.*;

import java.util.LinkedList;

public class main {
    public static void main(String[] args) {
        Casa casa1 = new Casa("1", "mi casa", "direccion 949", true);
        Casa casa2 = new Casa("2", "otra casa", "canelones 949", false);
        Casa casa3 = new Casa("3", "mas de otra casa", "durazon 949", false);
        Casa casa4 = new Casa("4", "nueva casa", "yaguaraon 949", true);
        Casa casa5 = new Casa("5", "ponele otra casa", "rembrandt 949", false);
        Casa casa6 = new Casa("6", "ultima casa", "picasso 949", false);

        Apartamento apto1 = new Apartamento("7", "mi apto", "algo 3050", (byte) 10, (byte) 3);
        Apartamento apto2 = new Apartamento("8", "mi otro apto", "apto 2036", (byte) 12, (byte) 3);
        Apartamento apto3 = new Apartamento("9", "nuevo apto", "nuevo 5086", (byte) 11, (byte) 3);
        Apartamento apto4 = new Apartamento("10", "casi nuevo apto", "durazno 2030", (byte) 4, (byte) 2);
        Apartamento apto5 = new Apartamento("11", "ultimo apto", "maldonado 892", (byte) 5, (byte) 3);

        LinkedList<Inmueble> inmuebles = new LinkedList<Inmueble>();
        inmuebles.add(casa1);
        inmuebles.add(casa2);
        inmuebles.add(casa3);
        inmuebles.add(casa4);
        inmuebles.add(casa5);
        inmuebles.add(casa6);

        inmuebles.add(apto1);
        inmuebles.add(apto2);
        inmuebles.add(apto3);
        inmuebles.add(apto4);
        inmuebles.add(apto5);

        System.out.println("Todos los inmuebles");
        listarInmuebles(inmuebles);
        System.out.println("Solo apartamentos");
        listarApartamentos(inmuebles);
        System.out.println("Solo casas con patios");
        listarCasaConPatio(inmuebles);
    }

    public static void listarInmuebles(LinkedList<Inmueble> inmuebles){
        for(Inmueble inmueble:inmuebles){
            System.out.println(inmueble);
        }
    }

    public static void listarApartamentos(LinkedList<Inmueble> inmuebles){
        for(Inmueble inmueble:inmuebles){
            if (inmueble instanceof Apartamento) {
                System.out.println(inmueble);
            }
        }
    }

    public static void listarCasaConPatio(LinkedList<Inmueble> inmuebles){
        for(Inmueble inmueble:inmuebles){
            if (inmueble instanceof Casa && ((Casa) inmueble).isTienePatio()) {
                System.out.println(inmueble);
            }
        }
    }
}
