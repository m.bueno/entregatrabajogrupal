package inmuebles;

public class Apartamento extends Inmueble {
    private byte cantidadPisos;
    private byte cantAptoPiso;

    public byte getCantidadPisos() {
        return cantidadPisos;
    }

    public void setCantidadPisos(byte cantidadPisos) {
        this.cantidadPisos = cantidadPisos;
    }

    public byte getCantAptoPiso() {
        return cantAptoPiso;
    }

    public void setCantAptoPiso(byte cantAptoPiso) {
        this.cantAptoPiso = cantAptoPiso;
    }

    public Apartamento(byte cantidadPisos, byte cantAptoPiso) {
        this.cantidadPisos = cantidadPisos;
        this.cantAptoPiso = cantAptoPiso;
    }

    public Apartamento(String id, String nombre, String direccion, byte cantidadPisos, byte cantAptoPiso) {
        super(id, nombre, direccion);
        this.cantidadPisos = cantidadPisos;
        this.cantAptoPiso = cantAptoPiso;
    }

    @Override
    public String toString() {
        return "Apartamento{" +
                "id='" + getId() + '\'' +
                ", nombre='" + getNombre() + '\'' +
                ", direccion='" + getDireccion() + '\'' +
                "cantidadPisos=" + cantidadPisos +
                ", cantAptoPiso=" + cantAptoPiso +
                '}';
    }
}
