package inmuebles;

public class Casa extends Inmueble {
    private boolean tienePatio;

    public boolean isTienePatio() {
        return tienePatio;
    }

    public void setTienePatio(boolean tienePatio) {
        this.tienePatio = tienePatio;
    }

    public Casa(boolean tienePatio) {
        this.tienePatio = tienePatio;
    }

    public Casa(String id, String nombre, String direccion, boolean tienePatio) {
        super(id, nombre, direccion);
        this.tienePatio = tienePatio;
    }

    @Override
    public String toString() {
        return "Casa{" +
                "id='" + getId() + '\'' +
                ", nombre='" + getNombre() + '\'' +
                ", direccion='" + getDireccion() + '\'' +
                "tienePatio=" + tienePatio +
                '}';
    }
}
